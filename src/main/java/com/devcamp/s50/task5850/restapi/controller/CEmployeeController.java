package com.devcamp.s50.task5850.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5850.restapi.model.CEmployee;
import com.devcamp.s50.task5850.restapi.repository.IEmployeeRepository;

@RestController
public class CEmployeeController {
    @Autowired
    IEmployeeRepository iEmployeeRepository;

    @CrossOrigin
    @GetMapping("/employees")
    public ResponseEntity<List<CEmployee>> getAllEmployees(){
        try {
            List<CEmployee> lisEmployees = new ArrayList<CEmployee>();
            iEmployeeRepository.findAll().forEach(lisEmployees::add);
            return new ResponseEntity<List<CEmployee>>(lisEmployees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
